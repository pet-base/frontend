import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { Customer } from '../entities/customer';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.scss']
})
export class CustomerViewComponent implements OnInit {

  customer: Customer;

  constructor(
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap;
    const customerId = routeParams.get('customerId');
    this.customerService.getCustomer(+customerId).subscribe(customer => this.customer = customer);
  }

  onDelete() {
    this.customerService.deleteCustomer(this.customer).subscribe(
      response => this.onSuccessfulDelete(response),
      error => this.onDeleteError(error)
    );
  }

  private onSuccessfulDelete(response) {
    console.log("Delete successful");
    console.log(response);
    this.router.navigate(['/customers/']);
  }

  private onDeleteError(error) {
    console.log("Something went wrong deleting this customer");
    console.log(error);
  }

}
