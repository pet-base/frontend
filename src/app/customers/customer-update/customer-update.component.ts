import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from '../customer.service';
import { Customer } from '../entities/customer';

@Component({
  selector: 'app-customer-update',
  templateUrl: './customer-update.component.html',
  styleUrls: ['./customer-update.component.scss']
})
export class CustomerUpdateComponent implements OnInit {

  customer: Customer;
  customerForm: FormGroup;

  constructor(
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
    ) { }

  ngOnInit(): void {
    const customerId = this.route.snapshot.paramMap.get('customerId');
    this.customerForm = this.fb.group({
      firstName: [''],
      middleName: [''],
      lastName: [''],
      phoneNumber: [''],
      emailAddress: ['']
    });
    this.customerService.getCustomer(+customerId).subscribe(
      customer => this.onUpdatedCustomerRetrieved(customer)
    );
  }

  onUpdatedCustomerRetrieved(customer) {
    this.customer = customer;
    this.customerForm.patchValue(customer);
  }

  onSubmit(){
    this.customerService.updateCustomer(this.customer.id, this.customerForm.value).subscribe(
      updatedCustomer => this.onSuccessfulUpdate(updatedCustomer),
      error => this.onError(error)
    );
  }

  private onSuccessfulUpdate(updatedCustomer) {
    console.log("Update successful");
    this.router.navigate([`/customers/${this.customer.id}`]);
  }

  private onError(error) {
    console.log("Something went wrong updating the customer");
    console.log(error);
  }

}
